/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mudara Bandaranayake
 */

/* Product class to hold data of each product which runs through each step*/
public class Product {
    
    /*Attributes of each product which passes through each step*/
    private String stepId="Sensor1";
    private String defectStatus="defect";
    
    /* Constructor of the product class which receives and set values to the attributes*/
    public Product(String stepId,String defectStatus) {
        stepId = this.stepId;
        defectStatus = this.defectStatus;
    }
    
    public Product() {
        
    }
    /*Getters and setters to be used to extract information and set values for each product which passes each step*/
    public String getstepId() {
        return stepId;
    }

    public String getdefectStatus() {
        return defectStatus;
    }
    
    public void setstepId(String sid) {
        this.stepId = sid;
    }

    public void setdefectStatus(String status) {
        this.defectStatus = status;
    }
    
    /*Returns the no.of items which passes through sensor 1 within one second*/
    public int getNumberofitemsS1(){
        Product product=new Product();
        int Nitems=0;
        List<Product> productList=new ArrayList<Product>();
        productList.add(product);
        Nitems=productList.size();
        return Nitems;
    }
    
    /*Returns the no.of defect items which passes through sensor 1 within one second*/
    public int getNumberofdefectsS1(){
        int Ndefects=0;
        Product product=new Product();
        List<Product> productList=new ArrayList<Product>();
        productList.add(product);
        for(Product prod : productList){
            if(prod.defectStatus=="defect"){
                Ndefects++;
            }
        }
        return Ndefects;
    }
    
    /*Returns the no.of items which passes through sensor 2 within one second*/
    public int getNumberofitemsS2(){
        Product product=new Product();
        int Nitems=0;
        List<Product> productList=new ArrayList<Product>();
        productList.add(product);
        Nitems=productList.size();
        return Nitems;
    }
    
    /*Returns the no.of defect items which passes through sensor 2 within one second*/
    public int getNumberofdefectsS2(){
        int Ndefects=0;
        Product product=new Product();
        List<Product> productList=new ArrayList<Product>();
        productList.add(product);
        for(Product prod : productList){
            if(prod.defectStatus=="defect"){
                Ndefects++;
            }
        }
        return Ndefects;
    }
    
    /*Returns the no.of items which passes through sensor 3 within one second*/
    public int getNumberofitemsS3(){
        Product product=new Product();
        int Nitems=0;
        List<Product> productList=new ArrayList<Product>();
        productList.add(product);
        Nitems=productList.size();
        return Nitems;
    }
    
    /*Returns the no.of defect items which passes through sensor 3 within one second*/
    public int getNumberofdefectsS3(){
        int Ndefects=0;
        Product product=new Product();
        List<Product> productList=new ArrayList<Product>();
        productList.add(product);
        for(Product prod : productList){
            if(prod.defectStatus=="defect"){
                Ndefects++;
            }
        }
        return Ndefects;
    }
    
    /*Returns the no.of items which passes through sensor 4 within one second*/
    public int getNumberofitemsS4(){
        Product product=new Product();
        int Nitems=0;
        List<Product> productList=new ArrayList<Product>();
        productList.add(product);
        Nitems=productList.size();
        return Nitems;
    }
    
    /*Returns the no.of defect items which passes through sensor 4 within one second*/
    public int getNumberofdefectsS4(){
        int Ndefects=0;
        Product product=new Product();
        List<Product> productList=new ArrayList<Product>();
        productList.add(product);
        for(Product prod : productList){
            if(prod.defectStatus=="defect"){
                Ndefects++;
            }
        }
        return Ndefects;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shift.management;

import java.io.BufferedReader; 
import java.io.IOException;
import java.nio.charset.StandardCharsets; 
import java.nio.file.Files; 
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList; 
import java.util.List;
/*
 * @author Hareendran
 */
public class CSVReaderInJava {
   
   public static void main(String[] args)
    {
        List<Shift> shifts = readBooksFromCSV("Data.csv");
        
         for (Shift b : shifts) {
            System.out.println(b);
    }  
}

private static List<Shift> readBooksFromCSV(String fileName)
{ 
    List<Shift> shifts = new ArrayList<>();
    Path pathToFile = Paths.get(fileName);

    // create an instance of BufferedReader
    // using try with resource, Java 7 feature to close resources
    try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) 
    {
        // read the first line from the text file 
        String line = br.readLine();

        // loop until all lines are read 
        while (line != null) {
            
            // use string.split to load a string array with the values from 
            // each line of
            // the file, using a comma as the delimiter 
            String[] attributes = line.split(",");

            Shift shift = createShift(attributes);
            
             // adding book into ArrayList
                shifts.add(shift);
        
            // read next line before looping 
            // if end of file reached, line would be null 
            line = br.readLine(); 
        }
    } catch (IOException ioe) {
            ioe.printStackTrace();
        }
          return shifts;
    }
private static Shift createShift(String[] metadata) 
{ 
    int shiftNo = Integer.parseInt(metadata[0]); 
    String employee1 = metadata[1];
    String employee2 = metadata[2]; 
    String employee3 = metadata[3];
    String manager = metadata[4]; 
    // create and return book of this metadata 
    return new Shift(shiftNo,employee1, employee2,employee3,manager); 
}
}

class Shift
{ 
    private int shiftNo;
    private String employee1;
    private String employee2;
    private String employee3; 
    private String manager;
    
    public Shift(int shiftNo, String employee1, String employee2, String employee3, String manager) 
    { 
        this.shiftNo = shiftNo;
        this.employee1 = employee1; 
        this.employee2 = employee2;
        this.employee3 = employee3;
        this.manager = manager;
    }

public int getShiftNo() 
{ 
    return shiftNo;
}
public void setShiftNo(int shiftNo) 
{ 
    this.shiftNo = shiftNo;
} 

 public String getEmployee1() {
        return employee1;
    }

public void setEmployee1(String employee1)
{ 
    this.employee1 = employee1;
} 

public String getEmployee2()
{
        return employee2;
}

public void setEmployee2(String employee2) 
{ 
    this.employee2 = employee2;
} 
public String getEmployee3()
{
        return employee3;
}

public void setEmployee3(String employee3) 
{ 
    this.employee3 = employee3;
} 

public String getManager()
{
        return manager;
}

public void setManager(String manager) 
{ 
    this.manager = manager;
} 
@Override
public String toString()
{
        return "[Shift No=" + shiftNo + ", Employee 1=" + employee1 + ", Employee 2=" + employee2 + ", Employee 3=" +employee3 +", Manager=" +manager+"]";
                 
} 
}


const express = require('express');
const router = express.Router();

const sensor1=require('../Schema/sensor1');
const sensor2=require('../Schema/sensor2');
const sensor3=require('../Schema/sensor3');
const sensor4=require('../Schema/sensor4');

var productDate=new Date();
var producttime=new Date();



router.get('/readings',function(req,res,next){
   res.send('All sensor readings')
});




//Getting all the data from sensor 1
router.get('/sensor1',function(req,res){
   sensor1.find(function(err,s1readings){
       res.json(s1readings);
   }) ;
});

//Getting all the data from sensor 2
router.get('/sensor2',function(req,res){
    sensor2.find(function(err,s2readings){
        res.json(s2readings);
    }) ;
});

//Getting all the data from sensor 3
router.get('/sensor3',function(req,res){
    sensor3.find(function(err,s3readings){
        res.json(s3readings);
    }) ;
});

//Getting all the data from sensor 4
router.get('/sensor4',function(req,res){
    sensor4.find(function(err,s4readings){
        res.json(s4readings);
    }) ;
});




module.exports = router;
const mongoose = require('mongoose');

const s4schema = mongoose.Schema;

var sensor4 = new s4schema({
    reading:{
        type: String,
        required: true
    },
    status:{
        type: String,
        required: true
    },
    date:{
        type: String,
        required: true
    },
    time:{
        type: String,
        required: true
    },
    prodid:{
        type:String,
        required:true
    }
});

const s1 = module.exports = mongoose.model('sensor4', sensor4);
const mongoose = require('mongoose');

const s1schema = mongoose.Schema;

var sensor1 = new s1schema({
    reading:{
        type: String,
        required: true
    },
    status:{
        type: String,
        required: true
    },
    date:{
        type: String,
        required: true
    },
    time:{
        type: String,
        required: true
    },
    prodid:{
        type:String,
        required:true
    }
});

const s1 = module.exports = mongoose.model('sensor1', sensor1);
const mongoose = require('mongoose');

const s3schema = mongoose.Schema;

var sensor3 = new s3schema({
    reading:{
        type: String,
        required: true
    },
    status:{
        type: String,
        required: true
    },
    date:{
        type: String,
        required: true
    },
    time:{
        type: String,
        required: true
    },
    prodid:{
        type:String,
        required:true
    }
});

const s1 = module.exports = mongoose.model('sensor3', sensor3);
var mongoose = require('mongoose');

const mongodb ='mongodb://localhost:27017/sensors';

function DBconnection()
{
    mongoose.connect(mongodb,{useNewUrlParser: true});

    mongoose.connection.on('connected',function()
    {
      console.log('Connected to sensors Database')
    }
    );

    mongoose.connection.on('error',function(err){
       if(err)
       {
           console.log("Error in DB connection, server.js module: "+err);
       }
    });

}

exports.dbconnect= DBconnection();
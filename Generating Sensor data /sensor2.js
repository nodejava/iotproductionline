const mongoose = require('mongoose');

const s2schema = mongoose.Schema;

var sensor2 = new s2schema({
    reading:{
        type: String,
        required: true
    },
    status:{
        type: String,
        required: true
    },
    date:{
        type: String,
        required: true
    },
    time:{
        type: String,
        required: true
    },
    prodid:{
        type:String,
        required:true
    }
});

const s2 = module.exports = mongoose.model('sensor2', sensor2);
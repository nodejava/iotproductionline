import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL; 
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.scene.chart.PieChart.Data;
import org.json.JSONArray;
import org.json.JSONObject;

public class JSONtoJava {

  
    public static void main(String[] args) {
        try{
         JJSONtoJava.call_me1();
         JSONtoJava.call_me2();
         JSONtoJava.call_me3();
         JSONtoJava.call_me4();
        } catch (Exception e) {
         e.printStackTrace();
       }
    }
    
    
    public static ArrayList call_me1() throws Exception {
	
	 //Get the URL of Sensor Data 
     String url = "http://localhost:3000/tasks/todo";
     URL obj = new URL(url);
	 
	 //Convert URL to String
     String object = url.toString();
	 
	 //Get the particular sensor data
     String object1 = object.substring(28, 32);
     HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	 
     // optional default is GET
	 
     con.setRequestMethod("GET");
	 
     //add request header
     con.setRequestProperty("User-Agent", "Mozilla/5.0");
     int responseCode = con.getResponseCode();
	 
     System.out.println("\nSending 'GET' request to URL : " + url);
     System.out.println("Response Code : " + responseCode);
	 
     BufferedReader in = new BufferedReader(
             new InputStreamReader(con.getInputStream()));
     String inputLine;
     StringBuffer response = new StringBuffer();
     while ((inputLine = in.readLine()) != null) {
     	response.append(inputLine);
     }
     in.close();
	 
    //print in String
    // System.out.println(response.toString());
    
     //Add the elements to the objects
     ArrayList<Product>Objects = new ArrayList<Product>();
	 //Read JSON response and print
     JSONArray myResponse = new JSONArray(response.toString());
     
     System.out.println("myresponse"+myResponse);
     System.out.println("result after Reading JSON Response");
     
	 //Convert to JSON Object to JAVA Object
     for(int i=0; i<myResponse.length();i++){
        JSONObject a = myResponse.getJSONObject(i);
        
        System.out.println("Sensor: " +object1);
        String pnam = a.getString("projectname");
        String tsk = a.getString("task");
        
       
        Product objects2 = new Product(object1,pnam);
        Objects.add(objects2);
        
       
       System.out.println("projectname- "+a.getString("projectname"));
       System.out.println("task- "+a.getString("task")); 
      }
	  
	  //Return the list of the objects
      return Objects;
     
}   
   
     public static ArrayList call_me2() throws Exception {
     String url = "http://localhost:3000/tasks/todo1";
     URL obj = new URL(url);
     String object = url.toString();
     String object1 = object.substring(28, 32);
     HttpURLConnection con = (HttpURLConnection) obj.openConnection();
     // optional default is GET
     con.setRequestMethod("GET");
     //add request header
     con.setRequestProperty("User-Agent", "Mozilla/5.0");
     int responseCode = con.getResponseCode();
     System.out.println("\nSending 'GET' request to URL : " + url);
     System.out.println("Response Code : " + responseCode);
     BufferedReader in = new BufferedReader(
             new InputStreamReader(con.getInputStream()));
     String inputLine;
     StringBuffer response = new StringBuffer();
     while ((inputLine = in.readLine()) != null) {
     	response.append(inputLine);
     }
     in.close();
     //print in String
    // System.out.println(response.toString());
    
     //Read JSON response and print
     JSONArray myResponse = new JSONArray(response.toString());
     ArrayList<Product>Objects = new ArrayList<Product>();
     System.out.println("myresponse"+myResponse);
     System.out.println("result after Reading JSON Response");
     
     for(int i=0; i<myResponse.length();i++){
        JSONObject a = myResponse.getJSONObject(i);
        
        System.out.println("Sensor: " +object1);
        String pnam = a.getString("projectname");
        String tsk = a.getString("task");
        

        Product objects2 = new Product(object1,pnam);
        Objects.add(objects2);
        
       
       System.out.println("projectname- "+a.getString("projectname"));
       System.out.println("task- "+a.getString("task")); 
      }
     return Objects;
}   
     
     public static ArrayList call_me3() throws Exception {
     String url = "http://localhost:3000/tasks/todo";
     URL obj = new URL(url);
     String object = url.toString();
     String object1 = object.substring(28, 32);
     HttpURLConnection con = (HttpURLConnection) obj.openConnection();
     // optional default is GET
     con.setRequestMethod("GET");
     //add request header
     con.setRequestProperty("User-Agent", "Mozilla/5.0");
     int responseCode = con.getResponseCode();
     System.out.println("\nSending 'GET' request to URL : " + url);
     System.out.println("Response Code : " + responseCode);
     BufferedReader in = new BufferedReader(
             new InputStreamReader(con.getInputStream()));
     String inputLine;
     StringBuffer response = new StringBuffer();
     while ((inputLine = in.readLine()) != null) {
     	response.append(inputLine);
     }
     in.close();
     //print in String
    // System.out.println(response.toString());
    
     //Read JSON response and print
     JSONArray myResponse = new JSONArray(response.toString());
     ArrayList<Product>Objects = new ArrayList<Product>();
     
     System.out.println("myresponse"+myResponse);
     System.out.println("result after Reading JSON Response");
     
     for(int i=0; i<myResponse.length();i++){
        JSONObject a = myResponse.getJSONObject(i);
        
        System.out.println("Sensor: " +object1);
        String pnam = a.getString("projectname");
        String tsk = a.getString("task");
        

        Product objects2 = new Product(object1,pnam);
        Objects.add(objects2);
        
       
       System.out.println("projectname- "+a.getString("projectname"));
       System.out.println("task- "+a.getString("task")); 
      }
     return Objects;
}   
      
     public static ArrayList call_me4() throws Exception {
     String url = "http://localhost:3000/tasks/todo";
     URL obj = new URL(url);
     String object = url.toString();
     String object1 = object.substring(28, 32);
     HttpURLConnection con = (HttpURLConnection) obj.openConnection();
     // optional default is GET
     con.setRequestMethod("GET");
     //add request header
     con.setRequestProperty("User-Agent", "Mozilla/5.0");
     int responseCode = con.getResponseCode();
     System.out.println("\nSending 'GET' request to URL : " + url);
     System.out.println("Response Code : " + responseCode);
     BufferedReader in = new BufferedReader(
             new InputStreamReader(con.getInputStream()));
     String inputLine;
     StringBuffer response = new StringBuffer();
     while ((inputLine = in.readLine()) != null) {
     	response.append(inputLine);
     }
     in.close();
     //print in String
    // System.out.println(response.toString());
    
     //Read JSON response and print
     ArrayList<Product>Objects = new ArrayList<Product>();
     JSONArray myResponse = new JSONArray(response.toString());
     
     System.out.println("myresponse"+myResponse);
     System.out.println("result after Reading JSON Response");
     
     for(int i=0; i<myResponse.length();i++){
        JSONObject a = myResponse.getJSONObject(i);
        
        System.out.println("Sensor: " +object1);
        String pnam = a.getString("projectname");
        String tsk = a.getString("task");
        

        Product objects2 = new Product(object1,pnam);
        Objects.add(objects2);
        
       
       System.out.println("projectname- "+a.getString("projectname"));
       System.out.println("task- "+a.getString("task")); 
      }
     return Objects;
}   
}
       
       
       
      }
} 
     
     
     

   
    
   }